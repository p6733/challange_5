package com.example.demo.repository;

import com.example.demo.model.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface UsersRepository extends JpaRepository<Users,Integer> {
    @Modifying
    @Query(value = "update users u set u.username= :username, u.password= " +
            ":password, u.email= :email where u.user_id= :user_id", nativeQuery = true)
    void updateUser(
            @Param("username") String username,
            @Param("password") String password,
            @Param("email") String email,
            @Param("user_id") Integer userId
    );

}
