package com.example.demo.repository;

import com.example.demo.model.Films;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional

public interface FilmsRepository extends JpaRepository<Films,Integer> {
    @Modifying
    @Query(value = "update films f set f.film_code= :film_code, f.film_name= :film_name, f.sedang_tayang_atau_tidak= :sedang_tayang_atau_tidak " +
            "where f.film_code= :film_code", nativeQuery = true)
    void updateFilm(@Param("film_code") Integer filmCode,
                    @Param("film_name") String filmName,
                    @Param("sedang_tayang_atau_tidak") String sedangTayangAtauTidak
    );



    @Modifying
    @Query(value = "delete from films where film_code =:film_code", nativeQuery = true)
    void deleteFilm(@Param("film_code") Integer filmCode);

    Films findByFilmCode(Integer filmCode);
}
