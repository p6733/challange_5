package com.example.demo.repository;


import com.example.demo.model.Schedules;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public interface SchedulesRepository extends JpaRepository<Schedules, Integer> {
    @Modifying
    @Query(value = "select * from films f " +
            "join schedules sc on sc.film_code = f.film_code " +
            "where f.film_name =:filmName ", nativeQuery = true)
    public List<Schedules> findByNamaFilm (String filmName);


    @Modifying
    @Query(value = "select * from films f " +
            "join schedules sc on sc.film_code = f.film_code " +
            "where f.sedang_tayang_atau_tidak =:sedangTayangAtauTidak ", nativeQuery = true)
    public List<Schedules> findByTayang (String sedangTayangAtauTidak);


}
