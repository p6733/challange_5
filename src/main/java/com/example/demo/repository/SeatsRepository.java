package com.example.demo.repository;

import com.example.demo.model.Seats;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SeatsRepository extends JpaRepository<Seats,String> {

}
