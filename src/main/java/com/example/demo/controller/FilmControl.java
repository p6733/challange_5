package com.example.demo.controller;

import com.example.demo.model.Films;
import com.example.demo.model.Schedules;
import com.example.demo.service.FilmsService;
import com.example.demo.service.SchedulesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController("/api")
public class FilmControl {
    @Autowired
    FilmsService filmsService;

    @Autowired
    SchedulesService schedulesService;



    //----------------------------bagian film service
    @PostMapping("/add-film")
    public String addFilm(@RequestBody Films films){
        filmsService.saveFilm(films.getFilmName(), films.getSedangTayangAtauTidak());
        return "add film sukses";

    }
    @PutMapping("/update-film")
    public void updateFilm(@RequestBody Films films){
        filmsService.updateFilm(films.getFilmCode(), films.getFilmName(), films.getFilmName());
    }
    @DeleteMapping("/delete-film/{id}")
    public void deleteFilmByCode(@PathVariable("id") Integer filmCode){
        filmsService.deleteFilm(filmCode);
    }


    @GetMapping("/show-film")
    public List<Films> showFilm(){
        List<Films> filmsList = filmsService.filmsList();
        System.out.println("Code Film\t\tNama Film\t\tStatus");
        filmsList.forEach(films -> {
            if(films.getSedangTayangAtauTidak().equals("tayang")){
                System.out.println(films.getFilmCode()+"\t\t\t"+films.getFilmName()+
                        "\t\t\t"+films.getSedangTayangAtauTidak());
            }
        });
        return filmsList;
    }








    //----------------------------bagian schedule dan film service
    public String addSchedule(Integer hargaTiket, String tanggalTayang,
                              String jamMulai, String jamSelesai, Integer filmCode){
        schedulesService.addSchedule(hargaTiket,tanggalTayang,jamMulai,jamSelesai, filmCode);
       return "iyau";
    }

    @GetMapping("/show-film-by-name/{filmName}")
    public List<Schedules> showFilmByName(@PathVariable("filmName") String filmName){
        List<Schedules> filmSchedule=schedulesService.getByNamaFilm(filmName);
        System.out.println("Nama Film\t\tTanggal Tayang\t\tJam Mulai\t\tJam Selesai\t\tHarga Tiket");
        filmSchedule.forEach(Films->{
            System.out.println(Films.getFilmCode().getFilmName()+"\t\t\t"+Films.getTanggalTayang()+"\t\t\t"+Films.getJamMulai()+
                    "\t\t\t"+Films.getJamSelesai()+"\t\t\tRp. "+Films.getHargaTiket());
                }
        );
        return filmSchedule;
    }
    @GetMapping("/show-film-by-status/{status}")
    public List<Schedules> showFilmByTayang(@PathVariable("status") String sedangTayangAtauTidak){
        List<Schedules>filmSchedule=schedulesService.getByTayang(sedangTayangAtauTidak);
        System.out.println("Nama Film\t\tTanggal Tayang\t\tJam Mulai\t\tJam Selesai\t\tHarga Tiket");
        filmSchedule.forEach(Films->{
                    System.out.println(Films.getFilmCode().getFilmName()+"\t\t\t"+Films.getTanggalTayang()+"\t\t\t"+Films.getJamMulai()+
                            "\t\t\t"+Films.getJamSelesai()+"\t\t\tRp. "+Films.getHargaTiket());
                }
        );
        return filmSchedule;

    }






    //----------------------------bagian film schedule, film, seats service

}
