package com.example.demo.controller;

import com.example.demo.model.Users;
import com.example.demo.service.UsersService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController(value = "/API")
public class UsersControl {
    @Autowired
    private UsersService usersService;
    @PostMapping("/add-user")
    public String userAdd( @RequestBody Users users){
        usersService.saveUser(users.getUsername(),users.getPassword(),users.getEmail());
      return "penambahan berhasil";
    }
    @PutMapping("/update-user")
    public String updateUser(@RequestBody Users users){
        usersService.updateUser(users.getUsername(), users.getPassword(), users.getEmail(), users.getUserId());
        return "update berhasil";
    }
    @DeleteMapping( value = "/delete-user/{id}")
    public String deleteUser(@PathVariable("id")  Integer userId){
        usersService.deleteUsers(userId);
        return "delete berhasil";
    }
    @GetMapping(value = "/show-user")
    public List<Users> showUser(){
        List<Users> usersList = usersService.userList();
        System.out.println("userId\tusername\t\tpassword\t\t\t\temail");
        usersList.forEach(users -> {
            System.out.println(users.getUserId()+"\t\t"+users.getUsername()+
                    "\t\t\t\t"+users.getPassword()+"\t\t"+users.getEmail());
        });
        return  usersList;
    }

}
