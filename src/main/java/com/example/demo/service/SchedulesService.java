package com.example.demo.service;

import com.example.demo.model.Films;
import com.example.demo.model.Schedules;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
@Service
public interface SchedulesService {
 public void addSchedule(Integer hargaTiket,String tanggalTayang,String jamMulai, String jamSelesai, Integer filmCode);
 public List<Schedules> getByNamaFilm(String filmName);
 public List<Schedules>getByTayang(String SedangTayangAtatuTidak);

}
