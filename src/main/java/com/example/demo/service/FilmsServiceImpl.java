package com.example.demo.service;

import com.example.demo.model.*;
import com.example.demo.repository.FilmsRepository;
import com.example.demo.repository.SchedulesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
@Service
public class FilmsServiceImpl implements FilmsService{
    @PersistenceContext
    private EntityManager em;
    @Autowired
    FilmsRepository filmsRepository;
    @Autowired
    SchedulesRepository schedulesRepository;
   // @Autowired
    //FilmsService filmsService;



    //----------------------------bagian film  service
    @Override
    public void saveFilm(String filmName, String sedangTayangAtauTidak) {
        Films films = new Films();
        films.setFilmName(filmName);
        films.setSedangTayangAtauTidak(sedangTayangAtauTidak);
        filmsRepository.save(films);
    }

    @Override
    public void updateFilm(Integer filmCode, String filmName, String sedangTayangAtauTidak) {
        filmsRepository.updateFilm(filmCode,filmName,sedangTayangAtauTidak);
    }
    public void deleteFilm(Integer filmCode){
        filmsRepository.deleteFilm(filmCode);
    }

    @Override
    public List<Films> filmsList() {
        return filmsRepository.findAll();
    }

    @Override
    public Films findByFilmCode(Integer filmCode) {
        return filmsRepository.findByFilmCode(filmCode);
    }



    //----------------------------bagian film schedule, film, seats service



}
