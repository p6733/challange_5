package com.example.demo.service;

import com.example.demo.model.Schedules;
import com.example.demo.model.SeatId;
import com.example.demo.model.Users;
import org.apache.catalina.User;
import org.springframework.stereotype.Service;

@Service
public interface SeatsService {
    public void addSeat( String studioName,Integer nomorKursi, Users userId, Schedules scheduleID);
    public void updateSeat(String studioName,Integer nomorKursi, Users userId, Schedules scheduleID);
    public void deleteSeat(String studioName,Integer nomorKursi);
}
