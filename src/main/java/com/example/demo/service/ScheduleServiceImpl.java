package com.example.demo.service;

import com.example.demo.model.Films;
import com.example.demo.model.Schedules;
import com.example.demo.repository.FilmsRepository;
import com.example.demo.repository.SchedulesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class ScheduleServiceImpl implements SchedulesService{
    @Autowired
    FilmsRepository filmsRepository;
    @Autowired
    SchedulesRepository schedulesRepository;
    @Override
    public void addSchedule(Integer hargaTiket, String tanggalTayang, String jamMulai, String jamSelesai, Integer filmCode) {
        Schedules schedules = new Schedules();
        schedules.setHargaTiket(hargaTiket);
        schedules.setTanggalTayang(tanggalTayang);
        schedules.setJamMulai(jamMulai);
        schedules.setJamSelesai(jamSelesai);
        Films films = filmsRepository.findByFilmCode(filmCode);
        films.setFilmCode(filmCode);
        schedulesRepository.save(schedules);
    }

    @Override
    public List<Schedules> getByNamaFilm(String filmName) {
        return schedulesRepository.findByNamaFilm(filmName);
    }

    @Override
    public List<Schedules> getByTayang(String SedangTayangAtatuTidak) {
        return schedulesRepository.findByTayang(SedangTayangAtatuTidak);
    }
}
