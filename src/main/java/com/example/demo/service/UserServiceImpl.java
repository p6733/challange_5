package com.example.demo.service;

import com.example.demo.model.Users;
import com.example.demo.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UsersService{
    @Autowired
    UsersRepository ur;
    @Override
    public void saveUser(String username, String password, String email) {
        Users users= new Users();
        users.setUsername(username);
        users.setPassword(password);
        users.setEmail(email);
        ur.save(users);
    }

    @Override
    public void updateUser(String username,  String password,String email,Integer userId) {
        ur.updateUser(username,password,email, userId);
    }

    @Override
    public void deleteUsers(Integer userId) {
        ur.deleteById(userId);
    }

    @Override
    public List<Users> userList() {
        return ur.findAll();
    }
}
