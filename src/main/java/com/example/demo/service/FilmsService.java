package com.example.demo.service;

import com.example.demo.model.Films;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public interface FilmsService {
    public void saveFilm( String filmName, String sedangTayangAtauTidak);

    public void updateFilm(Integer filmCode, String filmName, String sedangTayangAtauTidak);

    public  void deleteFilm(Integer filmCode);

    public List<Films> filmsList();

    public Films findByFilmCode(Integer filmCode);
}
