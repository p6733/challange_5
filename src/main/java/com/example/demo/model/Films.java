package com.example.demo.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Setter
@Getter
@Entity(name = "Films")
public class Films implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    @Column(name = "film_code")
    private Integer filmCode;

    @Column(name = "film_name")
    private String filmName;

    @Column(name = "sedang_tayang_atau_tidak")
    private String sedangTayangAtauTidak;
}
