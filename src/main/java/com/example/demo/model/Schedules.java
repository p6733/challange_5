package com.example.demo.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Setter
@Getter
@Entity(name = "Schedules")
public class Schedules implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    @Column(name = "schedule_id")
    private Integer scheduleID;

    @Column(name = "harga_tiket")
    private Integer hargaTiket;

    @Column(name = "tanggal_tayang")
    private  String tanggalTayang;

    @Column(name = "jam_mulai")
    private String jamMulai;

    @Column(name = "jam_selesai")
    private String jamSelesai;



    @ManyToOne
    @JoinColumn(name = "film_code")
    private Films filmCode;


}
