package com.example.demo.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
@Setter
@Getter
//@Embeddable
public class SeatId implements Serializable {
    @Id
    @Column(name ="studio_name" )
    private String studioName;

    @Id
    @Column(name ="nomor_kursi" )
    private String nomorKursi;


}
