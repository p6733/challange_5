package com.example.demo.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Setter
@Getter
@Entity
@IdClass(SeatId.class)
public class Seats implements Serializable {
    @Id
    @Column(name ="studio_name" )
    private String studioName;

    @Id
    @Column(name ="nomor_kursi" )
    private String nomorKursi;

    @ManyToOne
    @JoinColumn(name = "schedule_id")
    private Schedules schedulesID;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private Users userId;



}
