create table seats
(
    nomor_kursi int          not null,
    studio_name varchar(255) not null,
    schedule_id int          null,
    user_id     int          null,
    primary key (nomor_kursi, studio_name),
    constraint FK6k1o4fm1a3ipwguktr45y5wi4
        foreign key (user_id) references users (user_id),
    constraint FKfbw4qi0nr80b7o8kpb25gy2q7
        foreign key (schedule_id) references schedules (schedule_id)
);

