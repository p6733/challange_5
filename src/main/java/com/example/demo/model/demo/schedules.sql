create table schedules
(
    schedule_id    int          not null
        primary key,
    harga_tiket    int          null,
    jam_mulai      varchar(255) null,
    jam_selesai    varchar(255) null,
    tanggal_tayang varchar(255) null,
    code_film      int          null,
    film_code      int          null,
    constraint FK2p08sh9gs2q05xkdo5kxac77w
        foreign key (film_code) references films (film_code),
    constraint FKkus83fb2ukagrnlh1li062j12
        foreign key (code_film) references films (film_code)
);

